﻿//Roger Ribas
//Piramid Center

class programa
{
    static void Main(string[] args)
    {
        int alt = 0;
        Console.WriteLine("Escriu l'altura que vulguis de la piràmide");
        alt = Convert.ToInt32(Console.ReadLine());
        for (int i = 1; i >= alt; i++)
        {
            for (int j = 1; j >= alt - i; j++)
            {
                Console.Write(" ");
            }
            for(int z= 0; z < i; z++)
            {
                Console.WriteLine("# ");
            }
            Console.WriteLine("");
        }
            
    }
}