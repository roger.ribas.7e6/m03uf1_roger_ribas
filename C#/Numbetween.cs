﻿//Roger Ribas
//Number Between
//Montse/Eduard estoy teniendo problemas con el VSC y me esta dando algunos errores, por lo que no puedo comprobar que esté bien, mas hallá de lo 
//que yo crea. MSB3027|MSB5097 y varios mas. 

class Numbetween
{
    static void Main(string[] args)
    {
        int num = 0;
        Console.WriteLine("Escribe un numero entre el 1 y el 5");
        num = Convert.ToInt32(Console.ReadLine());
        while (num > 5 || num < 1)
        {
            Console.WriteLine("Incorrecto vuelve a escribir otro numero");
            num = Convert.ToInt32(Console.ReadLine());
        }
        Console.WriteLine("Tu numero es " + num);
    }
}